package com.gita.project1.d;

import java.util.Arrays;

public class TestD {

	public static int Solution(int[] a) {
		int min_value = -1;
		boolean flag = false;
		boolean result = false; 
		int negative_max = 0;
		
		for (int N = 0; N <= 20; N++) {
			if (!result) {
				flag = false;
				for (int i = 0; i < a.length; i++) {
					if(min_value == a[i] && a[i] < 0) {
						min_value = min_value-1;
						flag = true;
						break;
					}
				}
				if (flag) {
					result=false;
				} else {
					result =true;
					negative_max = min_value;
				}
			}
		}
		return negative_max;

	}

	public static void main(String[] args) {
		int[] arr = {3, 2, -5, -1, -2, 0};
		System.out.println(Solution(arr));
	}

}
