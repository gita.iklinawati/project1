package com.gita.project1.e;

import java.util.Scanner;

import javax.swing.text.MaskFormatter;

public class TestE {
	
	 public static String Mask(String original) {
		 
		 String mask = "";
		 char[] result = new char[original.length()];
		 for (int i = 0; i < original.length(); i++) {
			 result[i] = original.charAt(i);
		 }
		 
		 for (int i = 0; i < result.length; i++) {
			 if (result[i] == ' ') {
				 mask += ' ';
			 }
			 else if (i == 0) {
				 mask += result[i];
			 }
			 else if (result[i-1] == ' ') {
				 mask += result[i];
			 }
			 else if(result.length == i+1) {
				 mask += result[i];
			}else if (result[i+1] == ' ') {
				 mask += result[i];
			 }
			 else {
				mask += '*';
			}
			 
		 }
		 return mask;
	 
	 }


	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		Mask(input);
		System.out.println(Mask(input));
	}

}
