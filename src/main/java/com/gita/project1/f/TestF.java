package com.gita.project1.f;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
public class TestF {
	
	
	public static void main(String[] args) throws Exception {
		String url = "https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
        	response.append(inputLine);
        }
        in.close();
        String json = response.toString();
        String jsonObject =json.substring(1,json.length()-1);
//        System.out.println(response.toString());
        JSONObject myResponse = new JSONObject(jsonObject);
        System.out.println("response- "+myResponse.getString("country"));
	
	}
}
